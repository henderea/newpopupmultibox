﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PopupMultiboxV2
{
    public partial class MainClass : Form
    {
        public MainClass()
        {
            InitializeComponent();
            GenerateBGImage();
            BackgroundImage = bgImage;
        }

        private Bitmap bgImage;

        private void GenerateBGImage()
        {
            if (bgImage != null && bgImage.Width == Width && bgImage.Height == Height)
                return;
            bgImage = new Bitmap(Width, Height, PixelFormat.Format32bppArgb);
            GC.Collect();
            Graphics g = Graphics.FromImage(bgImage);
            g.CompositingQuality = CompositingQuality.HighSpeed;
            g.PixelOffsetMode = PixelOffsetMode.HighSpeed;
            g.SmoothingMode = SmoothingMode.HighSpeed;
            g.InterpolationMode = InterpolationMode.Low;
            g.Clear(Color.FromArgb(255, 231, 231, 231));
            Brush b = new SolidBrush(Color.FromArgb(255, 230, 230, 230));
            if (Height > 100)
            {
                g.FillRectangle(b, new Rectangle(50, 100, Width - 100, Height - 150));
                g.FillRectangle(b, new Rectangle(100, Height - 50, Width - 200, 50));
                g.FillPie(b, 50, Height - 100, 100, 100, 180, -90);
                g.FillPie(b, Width - 150, Height - 100, 100, 100, 0, 90);
                g.FillRectangle(Brushes.White, new Rectangle(60, 100, Width - 120, Height - 150));
                g.FillRectangle(Brushes.White, new Rectangle(100, Height - 50, Width - 200, 40));
                g.FillEllipse(Brushes.White, new Rectangle(60, Height - 90, 80, 80));
                g.FillEllipse(Brushes.White, new Rectangle(Width - 140, Height - 90, 80, 80));
            }
            g.FillRectangle(b, new Rectangle(50, 0, Width - 100, 100));
            g.FillPie(b, 0, 0, 100, 100, 90, 180);
            g.FillPie(b, Width - 100, 0, 100, 100, 90, -180);
            g.FillRectangle(Brushes.White, new Rectangle(50, 10, Width - 100, 80));
            g.FillEllipse(Brushes.White, new Rectangle(10, 10, 80, 80));
            g.FillEllipse(Brushes.White, new Rectangle(Width - 90, 10, 80, 80));
//            g.DrawImage(Properties.Resources.popupMultiboxIcon3_small, new Rectangle(Width - 130, 10, 80, 80));
        }
    }
}
