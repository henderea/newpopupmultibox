﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PopupMultiboxV2
{
    class Indexer
    {
        private volatile List<ResultItem> startMenuItems;

        private void buildStartMenuItems()
        {
            
        }

        private static void GetApps(string sDir, List<string> itms)
        {
            try
            {
                Regex tmp = new Regex(@".*\.lnk", RegexOptions.IgnoreCase);
                itms.AddRange(Directory.GetFiles(sDir).Where(f => tmp.IsMatch(f.Substring(f.LastIndexOf("\\") + 1))));
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    itms.Add(d + "\\");
                    GetApps(d, itms);
                }
            }
            catch { }
        }

        private void ReloadCache()
        {
            List<string> tmp1 = new List<string>(0);
            List<string> tmp2 = new List<string>(0);
            startMenuItems = new List<ResultItem>(0);
            string p1 = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu);
            string p2 = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu);
            GetApps(p1, tmp1);
            try
            {
                startMenuItems.Sort();
            }
            catch { }
            GetApps(p2, tmp2);
            List<string> fnd = new List<string>(0);
            foreach (string t in tmp1)
            {
                string fnd2 = t.Substring(p1.Length + 1);
                string dtxt;
                if (!fnd2.EndsWith("\\"))
                {
                    fnd2 = fnd2.Remove(fnd2.Length - 4);
                    dtxt = fnd2.Substring(fnd2.LastIndexOf("\\") + 1);
                }
                else
                    dtxt = fnd2.Substring(fnd2.Remove(fnd2.Length - 1).LastIndexOf("\\") + 1);
                startMenuItems.Add(new ResultItem(dtxt, t, fnd2));
                fnd.Add(fnd2);
            }
            foreach (string t in tmp2)
            {
                string fnd2 = t.Substring(p2.Length + 1);
                string dtxt;
                if (!fnd2.EndsWith("\\"))
                {
                    fnd2 = fnd2.Remove(fnd2.Length - 4);
                    dtxt = fnd2.Substring(fnd2.LastIndexOf("\\") + 1);
                }
                else
                    dtxt = fnd2.Substring(fnd2.Remove(fnd2.Length - 1).LastIndexOf("\\") + 1);
                if (fnd.Contains(fnd2)) continue;
                int ind = 0;
                foreach (string t1 in tmp1)
                {
                    string fnd3 = t1.Substring(p2.Length + 1);
                    if (!fnd3.EndsWith("\\"))
                        fnd3 = fnd3.Remove(fnd3.Length - 4);
                    if (fnd2.CompareTo(fnd3) < 0)
                        ind++;
                    else
                        break;
                }
                tmp1.Insert(ind, t);
                startMenuItems.Insert(ind, new ResultItem(dtxt, t, fnd2));
            }
        }
    }

    public class ResultItem
    {
        protected bool Equals(ResultItem other)
        {
            return string.Equals(DisplayText, other.DisplayText) && string.Equals(FullText, other.FullText) && string.Equals(EvalText, other.EvalText);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (DisplayText != null ? DisplayText.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (FullText != null ? FullText.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (EvalText != null ? EvalText.GetHashCode() : 0);
                return hashCode;
            }
        }

        public string DisplayText { get; set; }

        public string FullText { get; set; }

        public string EvalText { get; set; }

        public ResultItem() : this("", "") { }

        public ResultItem(string d, string f) : this(d, f, "") { }

        public ResultItem(string d, string f, string e)
        {
            DisplayText = d;
            FullText = f;
            EvalText = e;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((ResultItem)obj);
        }
    }
}
